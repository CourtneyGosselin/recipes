Chicken-Fried Chicken
=====================

Source: www.seriouseats.com/recipes/2015/07/chicken-fried-chicken-with-cream-gravy-recipe.html

Ingredients
-----------

 * 30mL paprika
 * 30mL black pepper
 * 10mL garlic powder
 * 10mL died oregano
 * 2.5mL cayenne pepper
 * 2 8oZ chicken breasts, cut and pounded into 4 cutlets
 * 250mL buttermilk
 * 1 large egg
 * 15mL + 10mL kosher salt, divided
 * 375mL all-pupose flour
 * 125mL cornstarch
 * 5mL baking power
 * 1L cooking oil, for frying

Directions
----------
 
 1. Combine paprika, black pepper, garlic powder, oregano, and cayenne pepper 
    in a small bowl and mix thoroughly
 2. Pound out chicken until about 1/4" thick
 3. Whisk buttermilk, egg, 15mL salt, and 30mL of the spice mixture in a large
    bowl
 4. Add chicken to wet mixture and turn to coat. Transfer bowl contents to a
    large freezer bag and refrigerate for at least 4 hours and up to overnight,
    flipping the bag occasionally to redistribute the contents and coat the
    chicken evenly
 5. Whisk together the flour, cornstarch, baking powder, 2 teaspoons salt, and
    the remaining spice mixture in a large bowl. Add 3 tablespoons of the
    marinade from the zipper-lock bag and work it into the flour with your
    fingertips. Remove the chicken from the bag, allowing the excess
    buttermilk to drip off. Drop the chicken into the flour mixture and toss
    and flip until thoroughly coated, pressing with your hand to get the
    flour to adhere in a thick layer. Shake the chicken over the bowl to remove
    excess flour, then transfer to a large plate.
 6. Adjust an oven rack to the middle position and preheat the oven to 175°F.
    Heat the shortening or oil to 425°F in a wok or 12-inch cast iron skillet
    over medium-high heat, about 6 minutes. Adjust the heat as necessary to
    maintain the temperature, being careful not to let the fat get any hotter.
 7. Carefully lower 2 pieces of chicken into the pan. Adjust the heat to
    maintain the temperature at 325°F for the duration of cooking. Fry the
    chicken pieces without moving them for 2 minutes. Carefully agitate the
    chicken with a wire-mesh spider or tongs, making sure not to knock off any
    breading, and cook until the bottom is a deep golden brown, about 3
    minutes. Carefully flip the chicken and continue to cook until the second
    side is golden brown, about 2 minutes longer.
 8. Transfer the chicken to a paper towel–lined plate to drain for 30
    seconds, flipping once, then transfer to a wire rack set on a rimmed baking
    sheet and transfer to the oven to keep warm. Repeat with the remaining 2
    pieces of chicken.
