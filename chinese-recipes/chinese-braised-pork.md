Chinese Braised Pork
=====================

Source: https://thewoksoflife.com/2014/04/shanghai-style-braised-pork-belly/

Ingredients
-----------
 - 330g Lean Pork Belly
 - 30ml Canola Oil
 - 15ml Sugar (Rock sugar is preferred)
 - 45ml Shaoxing Wine
 - 15ml Light Soy Sauce
 - 7.5ml Dark Soy Sauce
 - 500ml Water

Directions
----------
 1. Mix the oil and sugar in a bowl
 2. Mix the water, shaoxing wine, light soy sauce, and dark soy sauce in a bowl
 3. Cut the pork belly into 2cm cubes
 4. Bring a small pot of water to a boil
 5. Blanch the pork belly in the water for 2 minutes. Remove and drain the pork and set aside
 6. Melt the sugar water in a wok over low heat
 7. Raise the heat to medium high, add pork, and cook until lightly golden brown
 8. Add the oil and water mixture, reduce heat to a simmer. Cover for 45-60 minutes. Stir every 5-10 minutes
 9. When the sauce has reduced to a thick coating, remove from heat and serve.

Notes
-----
 - If the sauce doesn't reduce fast enough, you can remove some of the sauce, raise the heat, remove the cover, or some combination of those three
