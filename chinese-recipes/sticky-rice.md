Sticky Rice
=====================

Source: https://www.tablespoon.com/posts/how-to-make-sticky-rice-at-home

Ingredients
------------
 - 2 cup sticky rice
 - 2 1/2 cups water
 - 2.5ml salt

Directions
----------
 1. Measure all of the ingredients into a rice cooker
 2. Stir the rice to ensure the water and salt distribute evenly
 3. Soak the rice for 30 minutes to 4 hours
 4. Start the rice cooker and let it go until it turns off
