Oven-Fried Wings
=====================

Source: http://www.seriouseats.com/recipes/2010/02/the-best-buffalo-wings-oven-fried-wings-recipe.html

Ingredients
------------

 * 1700g chicken wings
 * 20g baking power
 * 20g salt
 * 100g unsalted butter

Directions
----------

 1. Line a rimmed baking sheet with aluminum foil and set a wire rack inside
 2. Carefully dry chicken wings with paper towels
 3. In a large bowl, combine wings with baking powder and salt and toss until thoroughly and evenly coated. Place on rack, leaving a slight space between each wing. Repeat until all wings are on the rack
 4. Place baking sheet with wings in refrigerator and allow to rest, uncovered, at least 8 hours and up to 24 hours.
 5. Adjust oven rack to upper-middle position and preheat oven to 450°F (230°C). Add chicken wings and cook for 20 minutes. Flip wings and continue to cook until crisp and golden brown, 15 to 30 minutes longer, flipping a few more times towards the end.
 6. Serve immediately with sauce or toppings of your choice
