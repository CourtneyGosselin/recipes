Buttermilk Pancakes
=====================

Source: http://www.seriouseats.com/2010/06/what-is-the-difference-between-baking-powder-and-baking-soda-in-pancakes.html

Ingredients
-----------

  * 285g all purpose flour
  * 5mL baking powder
  * 2mL baking soda
  * 5mL kosher salt
  * 5mL sugar
  * 2 large eggs, seperated
  * 710mL buttermilk
  * 60mL unsalted butter, melted

  Note: You can pre-mix and store the dry ingredients for 3 months
  
  Note: Buttermilk substitute can be 50mL melted butter, 615mL almond milk, 45mL
  vinegar / lemon juice, mixed and left in the refrigerator for 15 minutes

Directions
----------

 1. Combine dry ingredients in a bowl
 2. Whisk egg whites until stiff peak
 3. Whisk egg yolks and buttermilk together
 4. Slowly drizzle in melted butter to egg yolk buttermilk mixture while whisking
 5. Carefully fold in the egg whites to egg yolk buttermilk mixture with rubber
    spatula
 6. Pour the wet ingredients into the dry mix and fold until just combined
 7. Using medium heat, oil a pan with butter and cook the pancakes until
    bubbles appear on the surface and it's golden brown
